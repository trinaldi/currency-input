import React, { Component } from 'react';
import CurrencyInput from 'react-currency-input';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CurrencyInput decimalSeparator="," thousandSeparator="." prefix="R$ "/>
      </div>
    );
  }
}

export default App;
