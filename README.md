# Currency Input Demo

This repository demonstrates how the `react-currency-input` component behaves.

## Installation

```
$ git clone https://trinaldi@bitbucket.org/trinaldi/currency-input.git
$ cd currency-input
```

## Usage

```
$ npm install
$ npm start
```

## Configurations

Please refer to the 'source' section.

## Source
[Original component website](https://github.com/jsillitoe/react-currency-input)
